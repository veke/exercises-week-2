function calculator(operator, num1, num2) {
  if(operator === "+") {
    return num1 + num2;
  } else if (operator === "-") {
    return num1 - num2;
  } else if (operator === "*") {
    return num1 * num2;
  } else if (operator === "/") {
    return num1 / num2;
  } else return "Can't do that!";
}


console.log(calculator("+", 2, 3));
   