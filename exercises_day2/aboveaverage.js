const aboveAverage = (arr) => {
  const sum = arr
    .reduce((acc, el) => acc + el, 0);
  const average = sum/arr.length;
  const newArr = arr
    .map( (alkio) => alkio + average);
  return newArr;
};

console.log(aboveAverage([1, 5, 9, 3]));

//nyt ohjelma tulostaa taulukon jossa jokaiseen vanhan taulukon alkioon on summattu vanhan taulukon keskiarvo.