const checkExam = (right, student) => {
  let oikeat = 0;
  let väärät = 0;

  for(let i = 0; i < right.length; i++) {
    if (right[i] === student[i]) {
      oikeat ++;
    } else if (student[i] === "" ) {
      console.log; //piti laittaa jtn jotta ei ei ole tyhjä
    } else {
      väärät ++;
    }
  }

  const tulos = 4*oikeat - väärät;
  if (tulos > 0) return tulos;
  else return 0;
};


console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"]));
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""]));
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"]));
console.log(checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"]));