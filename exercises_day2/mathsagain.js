const steps = (n, askel) => {
  if (n % 2 === 0 && n !== 1) {
    n = n/2;
    askel ++;
    //console.log(n);
    steps(n, askel);
  } 
  else if (n % 2 !== 0 && n !== 1) {
    n = n * 3 + 1;
    askel ++;
    //console.log(n);
    steps(n, askel);
  }
  else {
    console.log(`reached number 1 after ${askel-1} steps`);
    //console.log(askel);
    //console.log(n);
    //return askel; MIKSI TÄMÄ RETURN EI TOIMI????
  }
};

const vaiheet = 0;
steps(3, vaiheet);
//console.log(steps(3, vaiheet));