const getVowelCount = (lause) => {
  const vowels = /[aeiou]/gi;
  const result = lause.match(vowels);
  const count = result.length;
  return count;
};


console.log(getVowelCount("abracadabra"));