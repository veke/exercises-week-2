const sana = process.argv[2];

let reversed = "";
for(let i = 1; i <= sana.length; i++) {
  reversed += sana[sana.length-i];
}

if (reversed === sana) {
  console.log(`Yes, "${sana}" is a palindrome`);
} else {
  console.log(`No, "${sana}" is not a palindrome`);
}