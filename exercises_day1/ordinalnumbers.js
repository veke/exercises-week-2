const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];
const lause = " competitor was ";
let arr = [];


for (let i = 0; i < competitors.length; i++) {
  if(i === 0) {
    arr.push(i+1 + ordinals[0] + lause + competitors[i]);
  } else if(i === 1) {
    arr.push(i+1 + ordinals[1] + lause + competitors[i]);
  } else if(i === 2) {
    arr.push(i+1 + ordinals[2] + lause + competitors[i]);
  } else {
    arr.push(i+1 + ordinals[3] + lause + competitors[i]);
  }
}

console.log(arr);