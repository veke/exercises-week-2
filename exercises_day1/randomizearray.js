const shuffle = (arr) => {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i+1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr;
};


const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
const shuffled = shuffle(array);
console.log(shuffled);
